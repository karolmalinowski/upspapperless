<?php

class UPSPapperless {

    const API_TESTING = "https://wwwcie.ups.com/rest/PaperlessDocumentAPI";
    const API_PRODUCTION = "https://filexfer.ups.com/rest/PaperlessDocumentAPI";
    const ENV_TESTING = "testing";
    const ENV_PRODUCTION = "production";
    const DOCUMENT_TYPE_AUTHORIZATION_FORM = "001";
    const DOCUMENT_TYPE_COMMERCIAL_INVOICE = "002";
    const DOCUMENT_TYPE_CERTIFICATE_OF_ORIGIN = "003";
    const DOCUMENT_TYPE_EXPORT_ACCOMPANYING_DOCUMENT = "004";
    const DOCUMENT_TYPE_EXPORT_License = "005";
    const DOCUMENT_TYPE_IMPORT_PERMIT = "006";
    const DOCUMENT_TYPE_ONE_TIME_NAFTA = "007";
    const DOCUMENT_TYPE_OTHER = "008";
    const DOCUMENT_TYPE_POWER_OF_ATTORNEY = "009";
    const DOCUMENT_TYPE_PACKING_LIST = "010";
    const DOCUMENT_TYPE_SED_DOCUMENT = "011";
    const DOCUMENT_TYPE_SHIPPER_LETTER_OF_INSTRUCTION = "012";
    const DOCUMENT_TYPE_DECALARATION = "013";

    private $UPSSecurity;
    private $shipperNumber;
    public $fileName = "Invoice.pdf";
    public $fileFormat = "pdf";
    public $documentType = self::DOCUMENT_TYPE_COMMERCIAL_INVOICE;
    public $env = self::ENV_PRODUCTION;
    public $lastUploadResponse;
    public $lastPushResponse;
    public $lastDeleteResponse;
    public $documentId;
    public $debug = false;

    function __construct($username, $password, $accessLicenseNumber, $shipperNumber) {
        $this->shipperNumber = $shipperNumber;
        $this->UPSSecurity = new UPSSecurity(new UsernameToken($username, $password), new ServiceAccessToken($accessLicenseNumber));
    }

    /**
     * 
     * @param string $linkToFile
     * @param string $shipmentIdentifier
     * @param string $shipmentDateAndTime Date in format Y-m-d-H.i.s
     * @param string $trackingNumber
     * @param string $shipmentType 1 = small package, 2 = freight
     * @return PushToImageRepositoryResponse
     * @throws Exception
     */
    public function uploadAndPush($linkToFile, $shipmentIdentifier, $shipmentDateAndTime, $trackingNumber, $shipmentType = "1") {
        $this->lastPushResponse = $this->lastUploadResponse = $this->documentId = null;

        $file = file_get_contents($linkToFile);
        if (!$file) {
            throw new Exception("Empty file or unable to open: " . $linkToFile);
        }

        $form = new UserCreatedForm($this->fileName, $this->fileFormat, $this->documentType, base64_encode($file));
        $uploadRequest = new UploadRequest($this->shipperNumber, $form);
        $upload = new Upload($this->UPSSecurity, $uploadRequest);

        $this->debug($upload, "Upload request");
        $this->lastUploadResponse = $uploadResponse = $this->_post($upload);
        $this->debug($uploadResponse, "Upload response");

        $uploadResponseCode = $uploadResponse->UploadResponse->Response->ResponseStatus->Code;
        if ($uploadResponseCode !== "1") {
            throw new Exception(json_encode($uploadResponse));
        }

        $this->documentId = $documentId = $uploadResponse->UploadResponse->FormsHistoryDocumentID->DocumentID;
        if (!$documentId) {
            throw new Exception("Empty DocumentID");
        }
        $FormsHistoryDocumentID = new FormsHistoryDocumentID($documentId);
        $pushRequest = new PushToImageRepositoryRequest(new Request(), $this->shipperNumber, $FormsHistoryDocumentID, $shipmentIdentifier, $shipmentDateAndTime, $shipmentType, $trackingNumber);
        $push = new Push($this->UPSSecurity, $pushRequest);

        $this->debug($push, "Push request");
        $this->lastPushResponse = $pushResponse = $this->_post($push);
        $this->debug($pushResponse, "Push response");

        $pushResponseCode = $pushResponse->PushToImageRepositoryResponse->Response->ResponseStatus->Code;
        if ($pushResponseCode !== "1") {
            throw new Exception(json_encode($pushResponse));
        }

        return $pushResponse;
    }

    public function delete($documentID) {
        $this->lastDeleteResponse = null;

        $deleteRequest = new DeleteRequest(new Request(), $this->shipperNumber, $documentID);
        $delete = new Delete($this->UPSSecurity, $deleteRequest);
        $this->debug($delete, "Delete request");
        $this->lastDeleteResponse = $deleteResponse = $this->_post($delete);
        $this->debug($deleteResponse, "Delete response");

        $deleteResponseCode = $deleteResponse->DeleteResponse->Response->ResponseStatus->Code;
        if ($deleteResponseCode !== "1") {
            throw new Exception(json_encode($deleteResponse));
        }

        return $deleteResponse;
    }

    private function debug($var, $label = null) {
        if ($this->debug) {
            echo "\n--- $label ---\n";
            print_r($var);
        }
    }

    private function _post($data, $dataEncoded = false, $rawResponse = false) {
        $url = $this->env === self::ENV_PRODUCTION ? self::API_PRODUCTION : self::API_TESTING;

        $postfields = $dataEncoded ? $data : json_encode($data, JSON_UNESCAPED_UNICODE);
        $this->debug($postfields, "Sending request to $url");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        $result = curl_exec($ch);
        $this->debug($result, "Response");
        curl_close($ch);

        // print_r($result);
        return $rawResponse ? $result : json_decode($result);
    }

}

class Delete {

    public $UPSSecurity;
    public $DeleteRequest;

    function __construct($UPSSecurity, $DeleteRequest) {
        $this->UPSSecurity = $UPSSecurity;
        $this->DeleteRequest = $DeleteRequest;
    }

}

class DeleteRequest {

    public $Request;
    public $ShipperNumber;
    public $DocumentID;

    function __construct($Request, $ShipperNumber, $DocumentID) {
        $this->Request = $Request === null ? new Request() : $Request;
        $this->ShipperNumber = $ShipperNumber;
        $this->DocumentID = $DocumentID;
    }

}

class Push {

    public $UPSSecurity;
    public $PushToImageRepositoryRequest;

    function __construct(UPSSecurity $UPSSecurity, PushToImageRepositoryRequest $PushToImageRepositoryRequest) {
        $this->UPSSecurity = $UPSSecurity;
        $this->PushToImageRepositoryRequest = $PushToImageRepositoryRequest;
    }

}

class PushToImageRepositoryRequest {

    public $Request;
    public $ShipperNumber;
    public $FormsHistoryDocumentID;
    public $ShipmentIdentifier;
    public $ShipmentDateAndTime;
    public $ShipmentType;
    public $TrackingNumber;

    function __construct(Request $Request, $ShipperNumber, FormsHistoryDocumentID $FormsHistoryDocumentID, $ShipmentIdentifier, $ShipmentDateAndTime, $ShipmentType, $TrackingNumber) {
        $this->Request = $Request === null ? new Request() : $Request;
        $this->ShipperNumber = $ShipperNumber;
        $this->FormsHistoryDocumentID = $FormsHistoryDocumentID;
        $this->ShipmentIdentifier = $ShipmentIdentifier;
        $this->ShipmentDateAndTime = $ShipmentDateAndTime;
        $this->ShipmentType = $ShipmentType;
        $this->TrackingNumber = $TrackingNumber;
    }

}

class FormsHistoryDocumentID {

    public $DocumentID;

    function __construct($DocumentID) {
        $this->DocumentID = $DocumentID;
    }

}

class Upload {

    public $UPSSecurity;
    public $UploadRequest;

    function __construct(UPSSecurity $UPSSecurity, UploadRequest $UploadRequest) {
        $this->UPSSecurity = $UPSSecurity;
        $this->UploadRequest = $UploadRequest;
    }

}

class UPSSecurity {

    public $UsernameToken;
    public $ServiceAccessToken;

    function __construct(UsernameToken $UsernameToken, ServiceAccessToken $ServiceAccessToken) {
        $this->UsernameToken = $UsernameToken;
        $this->ServiceAccessToken = $ServiceAccessToken;
    }

}

class UsernameToken {

    public $Username;
    public $Password;

    function __construct($Username, $Password) {
        $this->Username = $Username;
        $this->Password = $Password;
    }

}

class ServiceAccessToken {

    public $AccessLicenseNumber;

    function __construct($AccessLicensenumber) {
        $this->AccessLicenseNumber = $AccessLicensenumber;
    }

}

class UploadRequest {

    public $ShipperNumber;
    public $UserCreatedForm;
    public $Request;

    function __construct($ShipperNumber, UserCreatedForm $UserCreatedForm, Request $Request = null) {
        $this->ShipperNumber = $ShipperNumber;
        $this->UserCreatedForm = $UserCreatedForm;
        $this->Request = $Request === null ? new Request() : $Request;
    }

}

class UserCreatedForm {

    public $UserCreatedFormFileName;
    public $UserCreatedFormFileFormat;
    public $UserCreatedFormDocumentType;
    public $UserCreatedFormFile;

    function __construct($UserCreatedFormFileName, $UserCreatedFormFileFormat, $UserCreatedFormDocumentType, $UserCreatedFormFile) {
        $this->UserCreatedFormFileName = $UserCreatedFormFileName;
        $this->UserCreatedFormFileFormat = $UserCreatedFormFileFormat;
        $this->UserCreatedFormDocumentType = $UserCreatedFormDocumentType;
        $this->UserCreatedFormFile = $UserCreatedFormFile;
    }

}

class Request {

    public $Transactionreference;

    function __construct(TransactionReference $Transactionreference = null) {
        $this->Transactionreference = $Transactionreference === null ? new TransactionReference() : $Transactionreference;
    }

}

class TransactionReference {

    public $CustomerContext;

    function __construct($CustomerContext = null) {
        if (null !== $CustomerContext) {
            $this->CustomerContext = $CustomerContext;
        }
    }

}
