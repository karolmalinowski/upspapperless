<?php

$sampleShipmentRequest = <<<EOT
{
	"UPSSecurity": {
		"UsernameToken": {
			"Username": "{$UPS_USERNAME}",
			"Password": "{$UPS_PASSWORD}"
		},
		"ServiceAccessToken": {
			"AccessLicenseNumber": "{$UPS_ACCESS_LICENSE}"
		}
	},
	"ShipmentRequest": {
		"Request": {
			"RequestOption": "validate",
			"TransactionReference": {
				"CustomerContext": "Your Customer Context"
			}
		},
		"Shipment": {
			"Description": "Description",
			"Shipper": {
				"Name": "Shipper Name",
				"AttentionName": "Shipper Attn Name",
				"TaxIdentificationNumber": "123456",
				"Phone": {
					"Number": "1234567890",
					"Extension": "1"
				},
				"ShipperNumber": "{$UPS_SHIPPER_NUMBER}",
				"FaxNumber": "1234567890",
				"Address": {
					"AddressLine": "Myśliwiecka 3/5/7",
					"City": "Warszawa",
					"PostalCode": "00-459",
					"CountryCode": "PL"
				}
			},
			"ShipTo": {
				"Name": "Ship To Name",
				"AttentionName": "Ship To Attn Name",
				"Phone": {
					"Number": "1234567890"
				},
				"Address": {
					"AddressLine": "Myśliwiecka 3/5/7",
					"City": "Warszawa",
					"PostalCode": "00-459",
					"CountryCode": "PL"
				}
			},
			"ShipFrom": {
				"Name": "Ship From Name",
				"AttentionName": "Ship From Attn Name",
				"Phone": {
					"Number": "1234567890"
				},
				"FaxNumber": "1234567890",
				"Address": {
					"AddressLine": "Myśliwiecka 3/5/7",
					"City": "Warszawa",
					"PostalCode": "00-459",
					"CountryCode": "PL"
				}
			},
			"PaymentInformation": {
				"ShipmentCharge": {
					"Type": "01",
					"BillShipper": {
						"AccountNumber": "{$UPS_SHIPPER_NUMBER}"
					}
				}
			},
			"Service": {
				"Code": "11",
				"Description": "Standard"
			},
			"ShipmentRatingOptions": {
				"NegotiatedRatesIndicator": "0"
			},
			"Package": {
				"Description": "Description",
				"Packaging": {
					"Code": "02",
					"Description": "Description"
				},
				"Dimensions": {
					"UnitOfMeasurement": {
						"Code": "CM",
						"Description": "Centimeters"
					},
					"Length": "7",
					"Width": "5",
					"Height": "2"
				},
				"PackageWeight": {
					"UnitOfMeasurement": {
						"Code": "KGS",
						"Description": "Kilograms"
					},
					"Weight": "10"
				}
			}
		},
		"LabelSpecification": {
			"LabelImageFormat": {
				"Code": "GIF",
				"Description": "GIF"
			},
			"HTTPUserAgent": "Mozilla/4.5"
		}
	}
}
EOT;
