<?php
require_once "../UPSPaperless.php";

$UPS_USERNAME = "";
$UPS_PASSWORD = "";
$UPS_ACCESS_LICENSE = "";
$UPS_SHIPPER_NUMBER = "";

// create shipment to make the rest of example work
include "shipment_request.php";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://wwwcie.ups.com/rest/Ship");
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $sampleShipmentRequest);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
$result = curl_exec($ch);
curl_close($ch);
echo "\n--- Shipment response ---\n";
$shipmentResult = json_decode($result);
print_r($shipmentResult);

$createdShipmentIdentifier = $shipmentResult->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber;
$createdShipmentTrackingNumber = $shipmentResult->ShipmentResponse->ShipmentResults->PackageResults->TrackingNumber;


// init client
$ups = new UPSPapperless($UPS_USERNAME, $UPS_PASSWORD, $UPS_ACCESS_LICENSE, $UPS_SHIPPER_NUMBER);
// set ENV_PRODUCTION or ommit in live app
$ups->env = UPSPapperless::ENV_TESTING;
$ups->documentType = UPSPapperless::DOCUMENT_TYPE_OTHER; // default is 002 - Commercial Invoice
$ups->debug = true;

// send document to shipment
try {
    // In real app you should provide below variables from Shipment you want to upload document to
//    $linkToFile = "https://www.ups.com/media/en/invoice.pdf"; 
    $linkToFile = "empty.pdf";
    $shipmentIdentifier = $createdShipmentIdentifier; 
    $shipmentDateAndTime = date("Y-m-d-H.i.s");
    $trackingNumber = $createdShipmentTrackingNumber;
    $response = $ups->uploadAndPush($linkToFile, $shipmentIdentifier, $shipmentDateAndTime, $trackingNumber);
    
    echo "\n--- Upload response ---\n";
    print_r($ups->lastUploadResponse);
    echo "\n\n--- Push response ---\n";
    print_r($response);
    /**
     * For information or edit purposes you might want to save somewhere with shipment:
     * $ups->documentId
     * $response->PushToImageRepositoryResponse->FormsGroupID
     */
} catch (Exception $ex) {
    echo $ex->getMessage();
}

